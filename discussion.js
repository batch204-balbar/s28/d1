// CRUD Operations

// Insert Documents (CREATE)
/*
	Syntax:
	Insert One Document
		db.collectionaName.insertOne({
			"fieldA":"valueA",
			"fieldB":"valueB"
		})

	Insert Many Document
		db.collectionName.insertMany([
			{
				"fieldA": "valueA",
				"fieldB": "valueB"
			},
			{
				"fieldA": "valueA",
				"fieldB": "valueB"
			}

		])
*/

db.users.insertOne({
	"firstName": "Jane",
	"lastName": "Doe",
	"age": 21,
	"email": "janedoegmail.com",
	"department": "none"
	)}

db.users.insertMany([
	{
	"firstName": "Stephen",
	"lastName": "Hawking",
	"age": 76,
	"email": "stephehawking@mail.com",
	"department": "none"
	},
	{
	"firstName": "Neil",
	"lastName": "Armstrong",
	"age": 82,
	"email": "neilarmstrong@mail.com",
	"department": "none"
	}
])


db.courses.insertMany([
	{
	"name": "Javascript101",
	"price": 5000,
	"description": "Introduction to Javascript",
	"isActive": true
	},
	{
	"name": "HTML 101",
	"price": 2000,
	"description": "Introduction to HTML",
	"isActive": true
	},
	{
	"name": "CSS 101",
	"price": 2500,
	"description": "Introduction to CSS",
	"isActive": false
	}

])


// Find Documents (Read)
/*
Syntax:
	db.collectionName.find() - this will retrieve all our documents
	db.collectionName.find({"criteria": "value"}) - this will retrieve all
	our documents that will match with our criteria

	db.collectionName.findOne({}) will return the first document in our collection
	db.collectionName.findOne({"criteria": "value"}) - will return the first document in our collection that will match our criteria
*/

db.users.find();

db.users.find({
	"firstName": "Jane"
});

db.users.findOne({});

// Updating Documents (Update)
/*
	Syntax:
		db.collectionName.updateOne({
			"criteria":"value"
		},
			$set: {
				"fieldToBeUpdate": "updatedValue"
			}
		})
	
*/

db.collectionName.updateOne(
		{
			"firstName":"Test"
		},
		{
			$set: {
				"firstName": "Test",
				"lastName": "Test",
				"age": 0,
				"email": "Test@mail.com",
				"department": "Test",
				"status": "Test"
			}
		}
	);


db.collectionName.updateOne(
		{
			"firstName":"Test"
		},
		{
			$set: {
				"firstName": "Bill",
				"lastName": "Gates",
				"age": 65,
				"email": "billgates@mail.com",
				"department": "Operations",
				"status": "active"
			}
		}
	);

// Updating Multiple Documents
db.users.updateMany(
	{
		"department": "none"
	}, 
	{
		$set: {
			"department": "HR"
		}
	}
);

// Removing a field
db.users.updateOne({
	"firstName": "Bill"
	},
	{
		$unset: {
			"status": "active"
		}
});

db.users.updateMany(

	)

	db.courses.updateMany(
	{
		"courses": "none"
	}, 
	{
		$set: {
			"isActive": false
		}
	}
);

// Deleting Documents (DELETE)

/*
	Syntax:
		- db.collectionName.deleteOne({"criteria: "value})
*/

db.users.deleteOne({"firstName": "Test"})